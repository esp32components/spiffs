# SPIFFS for ESP32

 ### To use spiffs add:

```C
ESP_LOGI(TAG, "Initializing SPIFFS...");
// ==== Initialize the file system ====
printf("\r\n\n");
vfs_spiffs_register();
if (!spiffs_is_mounted) {
  ESP_LOGE(TAG, "SPIFFS not mounted!!!");
}
else {
  ESP_LOGI(TAG, "SPIFFS Mounted");
}
```


Very important is to include configuration for SPIFFS in Kconfig.projbuild in main

```Makefile
menu "Dream Sequencer CS-787"

config SPIFFS_BASE_ADDR
    hex "SPIFFS Base address"
    range 100000 1FFE000
    default 180000
    help
	Starting address of the SPIFFS area in ESP32 Flash

config SPIFFS_SIZE
    int "SPIFFS Size in bytes"
    range 262144 2097152
    default 1048576

config SPIFFS_LOG_BLOCK_SIZE
    int "SPIFFS Logical block size"
    range 4098 65536
    default 8192

config SPIFFS_LOG_PAGE_SIZE
    int "SPIFFS Logical page size"
    range 256 2048
    default 256
    help
	Set it to the phisycal page size og the used SPI Flash chip.
```
